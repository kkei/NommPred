from logging import NullHandler, getLogger, DEBUG, ERROR
from os import getenv
import pytest
import re
import subprocess
import sys


logger = getLogger(__name__)
logger.setLevel(DEBUG)


class TestRequirements:

    # Python
    def test_syspath(self, caplog):
        caplog.set_level(ERROR)

        pat_mod_name = re.compile(r'NommPred[/]*$')
        res = [pat_mod_name.search(x) is None for x in sys.path]
        bool_for_assertion = False in res
        if bool_for_assertion is True:
            assert True
        else:
            logger.error(sys.path)
            assert False

    def test_import_scripts(self):
        try:
            import scripts
        except ModuleNotFoundError as e:
            assert False

    def test_import_biopython(self):
        try:
            import Bio
            assert True
        except ModuleNotFoundError as e:
            assert False

    # Rscript command
    def test_existence_Rscript(self, caplog):
        caplog.set_level(ERROR)

        result_of_subprocess = subprocess.run(['Rscript', '--version'], shell=False, stdout=subprocess.PIPE,
                                              env={'PATH': getenv('PATH')})
        if result_of_subprocess.returncode != 0:
            logger.error(result_of_subprocess)
            logger.error(getenv('PATH'))
            assert False, '\"Rscript\" was not found.'

    # Environment Variables
    def test_MITOFATES_HOME_path(self, caplog):
        caplog.set_level(ERROR)
        assert getenv('MITOFATES_HOME', None) is not None

    def test_NOMMPRED_HOME_path(self, caplog):
        caplog.set_level(ERROR)
        assert getenv('NOMMPRED_HOME', None) is not None
