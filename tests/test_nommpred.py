from copy import copy
from itertools import combinations
from logging import getLogger, DEBUG, ERROR
from os import getenv
from pathlib import Path
import pytest
import re
import subprocess
from tempfile import NamedTemporaryFile, SpooledTemporaryFile

from scripts.extract_features import ExtractFeatures, extract_features
from NommPred import NommPred, np_predict

logger = getLogger(__name__)
logger.setLevel(DEBUG)


class TestExtractFeatures:

    # Class Initialization
    # pathes for: 1. NOMMPRED_HOME, 2. MITOFATES_HOME, 3. MITOFATES_EXTRACTOR, 4. Rscript, 5. point
    def test_init_success(self, caplog):
        # with a test for 'is_requirement_satisfied()'

        _args = dict(
            nommpred=Path(getenv('NOMMPRED_HOME', '')), 
            mitofates=Path(getenv('MITOFATES_HOME', '')), 
            mitofates_extractor=Path(getenv('MITOFATES_HOME', ''))/'bin'/'computeSVMFeatureForPresequence.pl', 
            rscript='Rscript', 
            point=Path(getenv('NOMMPRED_HOME', ''))/'model'/'point.csv'
            )
        _obj = ExtractFeatures(**_args)
        assert True

    def test_init_failure(self, caplog):
        l = list(range(0,5))
        num2key = {
            0: 'nommpred',
            1: 'mitofates',
            2: 'mitofates_extractor', 
            3: 'rscript', 
            4: 'point'
        }

        _args = dict(
            nommpred=Path(getenv('NOMMPRED_HOME', '')), 
            mitofates=Path(getenv('MITOFATES_HOME', '')), 
            mitofates_extractor=Path(getenv('MITOFATES_HOME', ''))/'bin'/'computeSVMFeatureForPresequence.pl', 
            rscript='Rscript', 
            point=Path(getenv('NOMMPRED_HOME', ''))/'model'/'point.csv'
            )

        def replace_to_none_and_make_instance(args_d: dict, pos: list):
            return_d = copy(args_d)
            for _i in pos:
                return_d[num2key[_i]] = 'None'

            try:
                _obj = ExtractFeatures(**return_d)
                return True
            except SystemError:
                return False

        results = []
        for i in range(1,6):
            comb = combinations(l, i)
            _res = [replace_to_none_and_make_instance(_args, x) for x in comb]
            results.append(_res)
        logger.debug(results)
        assert not True in results

    # Feature extraction method
    def test_extract_success(self, caplog):
        _obj = ExtractFeatures()
        res = _obj._extract('tests/examples.fasta')
        assert res.read(2) == '+1'

    def test_extract_failure(self, caplog):
        _obj = ExtractFeatures()
        with pytest.raises(subprocess.CalledProcessError):
            res = _obj._extract('tests/examples.fasta', perl='/usr/bin/perl')

    # Prediction method
    def test_prediction_success(self, caplog):
        _obj = ExtractFeatures()
        infile = _obj._extract('tests/examples.fasta')
        res = _obj._prediction(infile, 2)
        assert res == '0.9298168 0.8564346 0.9730299 0.007992314 0.0009611768 0.005926419 0.9943244 0.9929381 0.9894923 0.02722536 0.007946076 0.001435308'
    
    def test_prediction_failure(self, caplog):
        _obj = ExtractFeatures()
        infile = SpooledTemporaryFile()
        with pytest.raises(subprocess.CalledProcessError):
            res = _obj._prediction(infile, 2)

    # Cutoff method
    def test_cutoff_success(self, caplog):
        assert ExtractFeatures._cutoff(0.66, 0.5) is True

    def test_cutoff_failure(self, caplog):
        assert ExtractFeatures._cutoff(0.66, 0.67) is False
    
    # Convert pred to class method
    def test_convert_prob_to_class_success(self, caplog):
        _obj = ExtractFeatures()
        assert _obj.convert_score_to_class('0.9 0.6 0.3 0.0', 'MRO') == ['MRO', 'MRO', 'MRO', 'Other']

    # Output results method
    # # Extract sequence names from fasta method
    def test_extract_seq_name_from_fasta_success(self, caplog):
        assert ExtractFeatures._extract_seq_name_from_fasta(Path('tests/examples.fasta')) == ['GL50803_14519', 'GL50803_15196', 'GL50803_103891', 'GL50803_27293', 'GL50803_10608', 'GL50803_33866', 'TVAG_239660', 'TVAG_432650', 'TVAG_055320', 'O61068', 'A2FCD6', 'A2E8B1']

    def test_extract_seq_name_from_fasta_failure(self, caplog):
        assert ExtractFeatures._extract_seq_name_from_fasta(Path('pytest.ini')) == []
    
    def test_output_success(self, caplog):
        _obj = ExtractFeatures()
        features = _obj._extract('tests/examples.fasta')
        prob_text = _obj._prediction(features, 2)
        classes = _obj.convert_score_to_class(prob_text, 'MRO')
        seq_names = ExtractFeatures._extract_seq_name_from_fasta(Path('tests/examples.fasta'))

        spool_file = ExtractFeatures._output_formatter(seq_names, classes)
        spool_file.seek(0, 2)
        spool_file.tell() == 217

    def test_output_failure(self, caplog):
        seq_names = ExtractFeatures._extract_seq_name_from_fasta(Path('tests/examples.fasta'))
        spool_file = ExtractFeatures._output_formatter(seq_names, [])
        assert spool_file.read() == 'Seq_Name	class\n'

    def test_main(self, caplog):
        res = extract_features(Path('tests/examples.fasta'), 'MRO')
        assert len(res) == 216

    def test_cli(self, caplog):
        a = ExtractFeatures()
        cmd = ['python3', a.NOMMPRED_HOME/'scripts'/'extract_features.py', 
            '-i', a.NOMMPRED_HOME/'tests'/'examples.fasta',
            '-l', '2']
        _result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=False, check=True)
        assert re.match(r'Seq_Name\t', _result.stdout.decode()) is not None

class TestNommPred:

    def test_prediction(self, caplog):
        result = np_predict(infile='tests/examples.fasta', lineage=2)
        logger.error(result)
        assert re.match(r'Seq_Name	class\n', result)

    def test_cli(self, caplog):
        a = ExtractFeatures()
        cmd = ['python3', a.NOMMPRED_HOME/'NommPred.py', 
            '-i', a.NOMMPRED_HOME/'tests'/'examples.fasta',
            '-l', '2']
        _result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=False, check=True)
        assert re.match(r'Seq_Name\t', _result.stdout.decode()) is not None

    def test_stdin(self, caplog):
        a = ExtractFeatures()
        cmd = ['python3', a.NOMMPRED_HOME/'NommPred.py',
            '-l', '2']
        stdin = Path(a.NOMMPRED_HOME/'tests'/'examples.fasta')
        _result = subprocess.run(cmd, stdin=stdin.open(), stdout=subprocess.PIPE, shell=False, check=True)
        assert re.match(r'Seq_Name\t', _result.stdout.decode()) is not None

    @pytest.mark.skipif(True, reason='[]')
    def test_docker(self, caplog):
        a = ExtractFeatures()
        cmd = ['docker', 'run', '--rm', '-v', '${PWD}:/mnt',
            'nommpred', '-i', a.NOMMPRED_HOME/'tests'/'examples.fasta',
            '-l', '2']
        _result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=False, check=True)
        assert re.match(r'Seq_Name\t', _result.stdout.decode()) is not None
