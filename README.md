# NommPred

NommPred is a CUI program for the prediction of mitochondrial or mitochondrion-related organelle proteins from the protein sequences.

## Requirements
NommPred may only work on Linux based operating systems, such as Ubuntu or CentOS.
This instruction assumes that you are starting with Ubuntu system.

You should confirm that python3 is already installed on your system.

NommPred uses some external programs, so we provide a singularity definition file and a dockerfile for easily installation. 
You need to build the image from these files.

Alternatively, you can install NommPred without singularity or docker.

More than 2GB RAM is required for building. 
If your system did not have enough RAM, you should add temporarily swap space.

## Installation
### Singularity version
You need to install Singularity on your system.

On NommPred directory, type the following command in order to build the sinfularity image of NommPred. 
```
$ sudo singularity build nommpred.sif singularity.def
```

### Docker version
You need to install Docker on your system. We tested on Docker version 18.06.0-ce, build 0ffa825. To get Docker for Ubuntu, see [official website]( https://docs.docker.com/install/linux/docker-ce/ubuntu/).

To install python3:
```
$ sudo apt-get update
$ sudo apt-get install -y python3
```

To clone NommPred repository, type the following command on the terminal:
```
$ git clone https://gitlab.com/kkei/NommPred.git
```

On NommPred directory, type the following command in order to build the docker image of NommPred. It might take about 10 minutes.
```
$ sudo docker build -t nommpred .
```
If successful, you will see the following outputs:
```
Successfully built xxxxxxxxxxxx  
Successfully tagged nommpred:latest
```

### Non-docker version
We tested on Ubuntu 18.04 LTS. You need to set the environmental variable by editing, e.g., your .bash_profile.

#### Requirement packages
You need to install some packages. On ubuntu, you can install these packages by following command: 
```
$ sudo apt-get install -y \
  cpanminus \
  gcc \
  gfortran \
  g++ \
  libblas3 \
  libblas-dev \
  libcairo2 \
  libcurl4 \
  libgomp1 \
  libjpeg8 \
  liblapack3 \
  liblapack-dev \
  libpango-1.0-0 \
  libpangocairo-1.0-0 \
  libpaper-utils \
  libtcl8.6 \
  libtiff5 \
  libtk8.6 \
  libxt6 \
  libyaml-dev \
  make \
  python3 \
  python3-pip \
  unzip \
  xdg-utils \
  zip \
  wget
```

Next you need to install biopython by pip with following command:
```
$ python3 -m pip install biopython
```

#### Mitofates
NommPred use the Mitofates script for feature extraction.
You need to get Mitofates tarball,

```
$ wget -c http://mitf.cbrc.jp/MitoFates/program/MitoFates_1.2.tar.gz
$ tar zxvf MitoFates_1.2.tar.gz
```
and add the following line at the end of your .bash_profile:
```
export MITOFATES_HOME=/path/to/MitoFates
```

#### Perl modules
Perl6::Slurp and Math::Cephes modules are required by the MitoFates script.

```
$ cpanm Perl6::Slurp Math::Cephes
```

#### R, xgboost
NommPred uses R for the prediction. NommPred uses xgboost, a R package which requires R version >= 3.3.0.
To install R on your system, see the documentation on [R website](https://www.r-project.org/) and get R (version >= 3.3.0) from the download link.

We tested NommPred by R version 3.5.1 (compiled from source) and 3.4.4 (Precompiled binary for Ubuntu 16.04)

```
$ sudo apt-get install r-base-core
```

You should confirm the path to find Rscript, such as
```
$ which Rscript
/usr/bin/Rscript
```

After installation of R, you need to install xgboost. Type the following command:
```
$ R -q -e 'install.packages("xgboost", repos="https://cloud.r-project.org/")'
```

#### NommPred
To install NommPred from git repository, you should type the following command on the terminal:
```
$ git clone https://gitlab.com/kkei/NommPred.git
```

and add the following line at the end of your .bash_profile:
```
export NOMMPRED_HOME=/path/to/NommPred
```

## Running the test
If successful, you will get the following outputs.
```
Seq_Name	class
GL50803_14519   MRO
GL50803_15196   MRO
GL50803_103891  MRO
GL50803_27293   Other
GL50803_10608   Other
GL50803_33866   Other
TVAG_239660     MRO
TVAG_432650     MRO
TVAG_055320     MRO
O61068  Other
A2FCD6  Other
A2E8B1  Other
```
### Singularity version
To verify installation, finally, type the following command in the NommPred directory:
```
# if you build the docker image as "nommpred"
$ singularity run nommpred.sif -i tests/examples.fasta -l 2
```

### Docker version
To verify installation, finally, type the following command in the NommPred directory:
```
# if you build the docker image as "nommpred"
$ docker run --rm -v ${PWD}:/mnt nommpred -i tests/examples.fasta -l 2
```

### Non-docker version
For non-docker version, type the following command to run the test:

```
$ python3 NommPred.py -i tests/examples.fasta -l 2
```

## Usage
NommPred takes as input both the protein sequence in a FASTA file and taxonomic information.
Run the script as the follows:

```
usage: NommPred.py [-h] [-i INFILE] [-o OUTFILE] [-l LINEAGE] [--overwrite] [-d DOCKER_IMAGE_NAME]

NommPred v0.3: A CUI program for the prediction of mitochondrial or mitochondrion-related organelle proteins from the protein sequences. Testrun command: 'python3 NommPred.py -i tests/examples.fasta -l 2' or 'docker run --rm -v ${PWD}:/mnt nommpred -i tests/examples.fasta -l 2'

optional arguments:
  -h, --help            show this help message and exit
  -i INFILE, --infile INFILE
                        Infile must be a fasta format file. (default: infile.fasta)
  -o OUTFILE, --outfile OUTFILE
                        Path to output file. Default is None (stdout). (default: None)
  -l LINEAGE, --lineage LINEAGE
                        Default is option 1 (Mt). You can use the taxonomic group name instead of numeric character as options. 1) Mt, 2) MRO, 3) Piroplasma, 4) Chlorophyta, 5) Dictyostelium, 6) Plasmodium, 7) stramenopiles, 8) Toxoplasma, 9) Trypanosomatida (default: Mt)
  --overwrite           The output file will be overwritten if exists. (default: False)
  -d DOCKER_IMAGE_NAME, --docker_image_name DOCKER_IMAGE_NAME
                        Docker image (This option will be deprecated) (default: None)
```
```
# Command examples

$ singularity run nommpred.sif -i tests/examples.fasta -l 2

$ docker run --rm -v ${PWD}:/mnt nommpred -i tests/examples.fasta -l 2

$ ./NommPred -i query.fa
$ ./NommPred -i query.fa -l 1
$ ./NommPred -i query.fa -l Toxoplasma
```

You can get usage information by the following command in the terminal:
```
$ ./NommPred.py -h
```

## License

See the [LICENSE](LICENSE) file for details.

## Publication and Citation
Kume K, Amagasa T, Hashimoto T, Kitagawa H. 
 NommPred: Prediction of Mitochondrial and Mitochondrion-Related Organelle Proteins of Nonmodel Organisms.
 Evol Bioinform Online.
 2018;14:1176934318819835.
 doi:10.1177/1176934318819835.
 
## References
Chen T, Guestrin C. XGBoost: A scalable tree boosting system. eprint arXiv. 2016. doi:10.1145/2939672.2939785.

Fukasawa Y, Tsuji J, Fu SC, Tomii K, Horton P, Imai K.
 MitoFates: Improved Prediction of Mitochondrial Targeting Sequences and Their Cleavage Sites.
 Mol Cell Proteomics.
 2015;14(4):1113-26.
 doi:10.1074/mcp.M114.043083.
 
R Core Team. R: A language and environment for statistical computing. Vienna, Austria: R Foundation for Statistical Computing. 2018. https://www.R-project.org/.

