library(data.table)
library(xgboost)
options(scipen=100)

lineage = commandArgs(trailingOnly=TRUE)[1]

query <- fread('cat /dev/stdin', header=F, data.table=F)
query <- as.matrix(query[,-1])

model_name <- paste(Sys.getenv("NOMMPRED_HOME"),"/model/", lineage, ".model", sep="")

bst <- xgb.load(model_name)
pred <- predict(bst, query)

cat(pred)
