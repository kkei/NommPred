#!/usr/bin/env python3
import argparse
from Bio import SeqIO
import csv
from io import TextIOWrapper
from logging import NullHandler, getLogger, DEBUG, ERROR
from logging.config import dictConfig
from os import getenv
from pathlib import Path
import re
import subprocess
from tempfile import NamedTemporaryFile, SpooledTemporaryFile, _TemporaryFileWrapper
from typing import Union

# -----------------------------
# Logger

logger = getLogger(__name__)
logger.addHandler(NullHandler())


class ExtractFeatures:

	def __init__(self, nommpred=None, mitofates=None, mitofates_extractor=None, rscript=None, point=None) -> None:
		"""[summary]

		Args:
			nommpred (str, optional): Path to NommPred dir. Defaults to None.
			mitofates (str, optional): [description]. Defaults to None.
			mitofates_extractor (str, optional): [description]. Defaults to None.
			rscript (str, optional): Path to Rscript binary. Defaults to None.
			point (str, optional): Path to point.csv. Defaults to None.

		Raises:
			SystemError: If requirements were not satisfied.
		"""
		# PATH
		self.NOMMPRED_HOME = Path(nommpred) if nommpred is not None else Path(getenv('NOMMPRED_HOME', ''))
		self.MITOFATES_HOME = Path(mitofates) if mitofates is not None else Path(getenv('MITOFATES_HOME', ''))
		self.MITOFATES_EXTRACTOR = Path(mitofates_extractor) if mitofates_extractor is not None else Path(self.MITOFATES_HOME)/'bin'/'computeSVMFeatureForPresequence.pl'
		self.rscript = rscript or 'Rscript'
		self.point = Path(point) if point is not None else Path(self.NOMMPRED_HOME)/'model'/'point.csv'

		if self._is_requirement_satisfied() is False:
			raise SystemError(self._return_path_settings())

	def _is_requirement_satisfied(self) -> bool:
		_flag = True
		# NommPred
		if self.NOMMPRED_HOME.exists() is False:
			logger.error('NOMMPRED_HOME is not defined!')
			_flag = False

		# Mitofates
		if self.MITOFATES_HOME.exists() is False:
			logger.error('MITOFATES_HOME is not defined!')
			_flag = False
		else:
			if self.MITOFATES_EXTRACTOR.exists():
				logger.debug('Mitofates scripts: ' + self.MITOFATES_EXTRACTOR.absolute().as_posix())
			else:
				logger.error('Mitofates scripts are not found!')
				_flag = False

		# Rscript
		try:
			result_of_subprocess = subprocess.run([self.rscript, '--version'], shell=False, 
				stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, env={'PATH': getenv('PATH')})
			if result_of_subprocess.returncode != 0:
				logger.error("Rscript is something wrong!")
				_flag = False
		except FileNotFoundError:
			logger.error("Rscript is not found!")
			_flag = False

		# models
		if self.point.exists():
			logger.debug('Pointfile: ' + self.point.as_posix())
		else:
			logger.error('Pointfile (default: NommPred/model/point.csv) is not found!')
			_flag = False

		return _flag

	def _return_path_settings(self):
		return [self.NOMMPRED_HOME, self.MITOFATES_HOME, self.MITOFATES_EXTRACTOR, self.rscript, self.point]

	def _extract(self, infile: Union[str, Path], perl='perl') -> SpooledTemporaryFile:
		"""[summary]

		Args:
			infile (str): [description]
		"""

		feature_file = SpooledTemporaryFile(mode='w')
		try:
			cmd = [perl, self.MITOFATES_EXTRACTOR.as_posix(), '-c', '+1', Path(infile)]
			_result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=False, check=True)

			feature_file.write(_result.stdout.decode())
			logger.debug('Feature extraction ... Finished.')
		except subprocess.CalledProcessError:
			logger.error('Feature extraction Failed!')
			raise

		r_infile_hundle = self._convert_feature_file_to_r_infile(feature_file_hundle=feature_file)
		return r_infile_hundle

	@staticmethod
	def _convert_feature_file_to_r_infile(feature_file_hundle: SpooledTemporaryFile) -> SpooledTemporaryFile:
		r_infile_hundle = SpooledTemporaryFile(mode='w')
		feature_file_hundle.seek(0)
		pat = re.compile(r'[0-9]+:')
		for row in feature_file_hundle:
			replaced_row = re.sub(pat, '', row.rstrip())
			r_infile_hundle.write(replaced_row + '\n')
		r_infile_hundle.seek(0)

		return r_infile_hundle

	def _prediction(self, stdin: SpooledTemporaryFile, lineage: str, rscript='Rscript') -> str:
		"""[summary]

		Args:
			infile (SpooledTemporaryFile): [description]
			lineage (str): Lineage you want to use for prediction. Also see convert_lineage method. ex. '2', 'MRO'...

		Returns:
			str: [description]
		Notes:
			If the result of Prediction contains a WARNING statement from R, 
			it will automatically remove that and return it as the return.
		"""
		try:
			stdin.seek(0)
			cmd = [rscript, self.NOMMPRED_HOME/'scripts'/'pred.r', self._convert_lineage(lineage)]
			_result = subprocess.run(cmd, stdin=stdin, stdout=subprocess.PIPE, shell=False, check=True)
		except subprocess.CalledProcessError:
			logger.error('Prediction ... Failed.')
			raise
		return_text = _result.stdout.decode()
		logger.debug('Prediction ... Finished.')

		re_res = re.search(r'WARNING', return_text)
		if re_res is not None:
			return_text = return_text.split('\n')[1]
		logger.debug(return_text)
		return return_text

	@staticmethod
	def _cutoff(pred, custom):
		if pred > custom:
			return True
		else:
			return False

	@staticmethod
	def _convert_lineage(lineage):
		pred_name = ''
		if lineage in [1, "1", "Mt"]:
			pred_name = "Mt"
		elif lineage in [2, "2","MRO"]:
			pred_name = "MRO"
		elif lineage in [3, "3","Piroplasma"]:
			pred_name = "Piroplasma"
		elif lineage in [4, "4","Chlorophyta"]:
			pred_name = "Chlorophyta"
		elif lineage in [5, "5","Dictyostelium"]:
			pred_name = "Dictyostelium"
		elif lineage in [6, "6","Plasmodium"]:
			pred_name = "Plasmodium"
		elif lineage in [7, "7","stramenopiles"]:
			pred_name = "stramenopiles"
		elif lineage in [8, "8","Toxoplasma"]:
			pred_name = "Toxoplasma"
		elif lineage in [9, "9","Trypanosomatida"]:
			pred_name = "Trypanosomatida"
		else:
			raise Exception("Organismal info ... Unknown.")
		
		return pred_name

	def convert_score_to_class(self, score_text: str, lineage: str):
		"""[summary]

		Args:
			score_text (str): Prediction results. ex. '0.9298168 0.8564346'
			lineage (str): Lineage name converted by convert_lineage method. ex. 'MRO'

		Returns:
			[type]: [description]
		"""
		probs = score_text.split(" ")
		return_list = []
		custom_cutoff_csv = []
		custom = ''
		
		with self.point.open('r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				custom_cutoff_csv.append(row)
				
		for row in custom_cutoff_csv:
			if row['lineage'] == lineage:
				custom = row['minimax_med']
				break
		
		if lineage == 'MRO':
			out_lineage_name = 'MRO'
		else:
			out_lineage_name = 'Mt'

		for prob in probs:
			res_class = out_lineage_name if self._cutoff(prob, custom) else 'Other'
			return_list.append(res_class)
		return return_list
		
	@staticmethod
	def _output_formatter(seqnames: list, classes: list) -> SpooledTemporaryFile:
		w = SpooledTemporaryFile(mode='w')
		w.write('Seq_Name\tclass\n')
		for el_seqname, el_class in zip(seqnames, classes):
			w.write('{}\t{}\n'.format(el_seqname, el_class))
		w.seek(0)
		return w

	@staticmethod
	def _extract_seq_name_from_fasta(infile: Union[str, Path]) -> list:
		return_list = []
		with Path(infile).open('r') as f:
			for record in SeqIO.parse(f, 'fasta'):
				return_list.append(record.description)
		return return_list


def extract_features(infile: Union[str, Path], lineage) -> str:

	_obj = ExtractFeatures()
	features = _obj._extract(infile)
	seq_names = ExtractFeatures._extract_seq_name_from_fasta(infile)

	prob_text = _obj._prediction(features, 2)
	classes = _obj.convert_score_to_class(prob_text, _obj._convert_lineage(lineage))

	spool_file = ExtractFeatures._output_formatter(seq_names, classes)
	content = spool_file.read().rstrip()

	return content

#-----------------------------
# MAIN

def main(args):

	result = extract_features(infile=args.infile, lineage=args.lineage)
	
	if (args.outfile is not None) and (Path(args.outfile).exists() is False):
		with Path(args.outfile).open('w') as w:
			w.write(result)
	elif (args.outfile is not None) and (Path(args.outfile).exists() is True):
		print('# Outfile already exists.')
		print(result)
	else:
		print(result)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(add_help=True)
	parser.add_argument('-i','--infile', type=str, action='store')
	parser.add_argument('-o','--outfile', type=str, action='store')
	parser.add_argument('-l', '--lineage', type=str, action='store')
	args = parser.parse_args()

	main(args)
