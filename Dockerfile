FROM ubuntu:18.04
MAINTAINER Keitaro KUME <2588019-kkei@users.noreply.gitlab.com>

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get install -y \
  cpanminus \
  gcc \
  gfortran \
  g++ \
  libblas3 \
  libblas-dev \
  libcairo2 \
  libcurl4 \
  libgomp1 \
  libjpeg8 \
  liblapack3 \
  liblapack-dev \
  libpango-1.0-0 \
  libpangocairo-1.0-0 \
  libpaper-utils \
  libtcl8.6 \
  libtiff5 \
  libtk8.6 \
  libxt6 \
  libyaml-dev \
  make \
  python3 \
  python3-pip \
  r-base-core \
  unzip \
  xdg-utils \
  zip \
  wget \
 && python3 -m pip install -U pip \
 && python3 -m pip install cython biopython \
 && wget http://mitf.cbrc.jp/MitoFates/program/MitoFates_1.2.tar.gz \
 && tar zxvf MitoFates_1.2.tar.gz --remove-file \
 && R -q -e 'install.packages("xgboost", repos="https://cloud.r-project.org/")' \
 && mkdir /mnt/shared_dir \
 && cpanm Perl6::Slurp Math::Cephes \
 && apt-get clean && rm  -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

ENV PYTHONPATH /usr/local/lib/python3.6/dist-packages:$PYTHONPATH
ENV NOMMPRED_HOME /NommPred
ENV MITOFATES_HOME /MitoFates
ENV DEBIAN_FRONTEND=""

COPY . ${NOMMPRED_HOME}/
WORKDIR /mnt

ENTRYPOINT ["python3", "/NommPred/NommPred.py"]
