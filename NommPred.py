#!/usr/bin/env python3
"""
"""
import argparse
from io import TextIOWrapper
from logging import getLogger
from pathlib import Path
import shutil
import subprocess
import sys
import tempfile
from typing import Union
from uuid import uuid4

from scripts.extract_features import extract_features

# -----------------------------
# Logger
# dictConfig(json.loads(open(log_setting_file_path.as_posix()).read()))
logger = getLogger(__name__)


# -----------------------------
# FUNC
class NommPred:

    def __init__(self):
        pass

    @staticmethod
    def run_without_docker(infile: Union[str, Path], lineage: Union[str, int]) -> str:
        result = extract_features(infile=infile, lineage=lineage)
        logger.info('Job ... Finished.')
        return result

    @staticmethod
    def run_with_docker(infile, lineage, docker_image_name):
        # TODO be deprecated
        logger.warning(
            'This command will be deprecated in the near future. '
            'As an alternative, please consider simply running it from docker or singularity, '
            'see the examples in the README.')
        with tempfile.TemporaryDirectory(dir=Path('./').absolute()) as tempdir_name:
            tempfile_name = str(uuid4())
            tempfile_path = Path(tempdir_name)/tempfile_name
            logger.debug('Copy seq file: [{}] into [{}]'.format(infile, str(tempfile_path)))
            shutil.copy(infile, str(tempfile_path))
        
            docker_command = " ".join(['docker run --rm',
                '-v', tempdir_name + ':/mnt',
                docker_image_name,
                '-i', '/mnt/' + tempfile_name,
                '-l', lineage])
            try:
                return_subproc = subprocess.run(docker_command, stdout=subprocess.PIPE, shell=False, check=True)
                logger.debug(return_subproc)
                return_stdout = return_subproc.stdout.decode().rstrip()
                logger.info('Docker job ... Finished.')
            except subprocess.CalledProcessError as e:
                logger.error(e)
                logger.error('Docker job ... Failed.')
                raise
            return return_stdout


def np_predict(infile: Union[str, Path], lineage='Mt', docker_image_name=None):
    obj = NommPred()
    if docker_image_name is None:
        result = obj.run_without_docker(infile=infile, lineage=lineage)
    else:
        result = obj.run_with_docker(infile=infile, lineage=lineage)
    return result


# -----------------------------
# MAIN
def main(args):

    # Init
    if args.outfile is not None:
        if Path(args.outfile).exists():
            if args.overwrite:
                Path(args.outfile).unlink()
                logger.info(str(Path(args.outfile)) + ' was deleted. OK.')
            else:
                logger.error(str(Path(args.outfile)) + ' ... Already exists. NG.')
                raise FileExistsError
        else:
            logger.debug(str(Path(args.outfile)) + ' ... Not found. OK.')

    # If infile is stdin, create a temporary file because the integrated script does not accept stdin. 
    if isinstance(args.infile, str):
        infile = args.infile
    elif isinstance(args.infile, TextIOWrapper):
        nt = tempfile.NamedTemporaryFile(mode='w')
        nt.write(args.infile.read())
        nt.seek(0)
        infile = nt.name
    else:
        raise AttributeError

    # Prediction
    result = np_predict(infile=infile,
                        lineage=args.lineage,
                        docker_image_name=args.docker_image_name)

    # Write outfile
    if (args.outfile is not None) and (Path(args.outfile).exists() is False):
        with Path(args.outfile).open('w') as w:
            w.write(result)
    else:
        print(result)

    # Finalization
    try:
        nt.close()
    except NameError:
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='NommPred v0.3: A CUI program for the prediction of mitochondrial or mitochondrion-related organelle proteins from the protein sequences. ' +\
        'Testrun command: \'python3 NommPred.py -i tests/examples.fasta -l 2\' or ' \
        '\'docker run --rm -v ${PWD}:/mnt nommpred -i tests/examples.fasta -l 2\'',
        add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-i', '--infile', type=argparse.FileType('r'), default=sys.stdin, 
        help='Infile must be a fasta format file.')
    parser.add_argument('-o', '--outfile', type=str, action='store', help='Path to output file. Default is None (stdout).')
    parser.add_argument('-l', '--lineage', type=str, action='store', default='Mt', help='Default is option 1 (Mt). You can use the taxonomic group name instead of numeric character as options.\
        1) Mt,\
        2) MRO,\
        3) Piroplasma,\
        4) Chlorophyta,\
        5) Dictyostelium,\
        6) Plasmodium,\
        7) stramenopiles,\
        8) Toxoplasma,\
        9) Trypanosomatida')

    # parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('--overwrite', action='store_true', help='The output file will be overwritten if exists.')
    parser.add_argument('-d', '--docker_image_name', type=str, action='store', help='Docker image')

    args = parser.parse_args()

    main(args)
